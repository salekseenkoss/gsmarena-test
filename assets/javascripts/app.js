//= require ./angular.min.js
//= require ./angular-route.min.js
//= require ./angular-resource.min.js

//= require_self

//= require ./config/router.js
//= require_tree ./devices
//= require_tree ./device_detail

var deviceFinderApp = angular.module('deviceFinderApp', [
  'ngRoute',
  'devices',
  'deviceDetail'
]);

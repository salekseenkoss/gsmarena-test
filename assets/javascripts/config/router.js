angular.
  module('deviceFinderApp').
  config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/', {
          template: '<devices></devices>'
        }).
        when('/detail/:resource/:deviceId', {
          template: '<device-detail></device-detail>'
        }).
        otherwise('/');
    }
  ]);

require 'faraday'

class GSMArenaClient
  DEFAULT_HOST = 'http://www.gsmarena.com/'
  INDEX_PATH = 'results.php3'

  attr_reader :conn

  def initialize
    @conn = Faraday.new(url: DEFAULT_HOST) do |faraday|
      faraday.request  :url_encoded
      faraday.adapter  Faraday.default_adapter
    end
  end

  def search(name)
    html = get(INDEX_PATH, { 'sName' => name })
    GSMArena::Search.parse(html)
  end

  def detail(device_path)
    html = get(device_path, {})
    GSMArena::Detail.parse(html)
  end

  private

  def get(url, params)
    handle_response(conn.get(url, params))
  end

  def handle_response(response)
    case response.status
    when 200
      response.body
    when 404
      fail GSMArena::NotFound
    when 500
      fail GSMArena::InternalServerError
    end
  end
end

class Finder
  attr_reader :query, :resource, :handler_class

  def initialize(resource, query)
    @query = query
    @resource = resource
    load_parser_class
  end

  def self.search(*args)
    finder = new(*args)
    finder.handler_class.search(finder.query)
  end

  def self.detail(*args)
    finder = new(*args)
    finder.handler_class.detail(finder.query)
  end

  private

  def load_parser_class
    return unless ParserRegistry.respond_to?(resource)
    @handler_class = ParserRegistry.send(resource)
  end
end

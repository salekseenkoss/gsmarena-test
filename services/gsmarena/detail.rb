require 'nokogiri'

module GSMArena
  class Detail
    DESCR_TAG = '#specs-list'
    TITLE_TAG = '.specs-phone-name-title'
    IMAGE_TAG = '.specs-photo-main > a > img'

    attr_reader :html, :parsed_dom, :device
    
    def initialize(html)
      @html = html
      @device = { categories: {} }
    end

    def self.parse(*args)
      searcher = new(*args)
      searcher.parse
    end

    def parse
      @parsed_dom = Nokogiri::HTML(html)
      append_device_info(parsed_dom)
      device
    end

    private

    def append_device_info(html)
      device_info = html.css(DESCR_TAG)
      
      device[:title] = html.css(TITLE_TAG).text
      device[:image_url] = html.css(IMAGE_TAG).first['src']
      device[:description] = parse_device_description(device_info)
      
      parse_table(device_info)
    end

    def parse_device_description(html)
      html.css('p').text
    end

    def parse_table(html)
      tables = html.css('table')
      tables.each do |table|
        rows = table.children.css('tr')

        category = parse_category(rows)
        descr = parse_category_descr(rows)

        append_category(category, descr)
      end
    end

    def parse_category(html)
      html.first.children.css('th').text
    end

    def parse_category_descr(html)
      block = {}
      _current = ""

      html.each do |item|
        columns = item.css('td')

        next unless columns[0]

        technology = columns[0].children.first.text
        value = columns[1].text
        
        if content_exists?(technology)
          _current = technology
          block[_current] = value
        else
          _current.empty? ? block[technology] = value : block[_current] += "\n#{value}"
        end
      end

      block
    end

    def append_category(category, data)
      device[:categories][category] = data
    end

    def content_exists?(word)
      !word.to_s.gsub(/\W/, "").empty?
    end
  end
end

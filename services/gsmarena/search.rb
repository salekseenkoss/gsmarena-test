require 'nokogiri'
require 'pry-byebug'
module GSMArena
  class Search
    ITEMS_LIST_CSS = '#review-body ul > li > a'
    TITLE_CSS = 'strong > span'

    attr_reader :html, :parsed_dom, :devices
    
    def initialize(html)
      @html = html
      @devices = []
    end

    def self.parse(*args)
      searcher = new(*args)
      searcher.parse
    end

    def parse
      @parsed_dom = Nokogiri::HTML(html)
      
      items_list.each do |item|
        devices << device_info(item)
      end

      devices
    end

    private

    def items_list
      parsed_dom.css(ITEMS_LIST_CSS)
    end

    def parse_title(item)
      item.css(TITLE_CSS).children.map { |e| e.text }.join(" ")
    end

    def parse_image_url(item)
      item.css('img').first['src']
    end

    def device_info(item)
      {
        detail_path: item['href'],
        title: parse_title(item),
        image_url: parse_image_url(item)
      }
    end
  end
end

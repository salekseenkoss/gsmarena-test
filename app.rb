require 'sinatra'
require 'slim'

require 'sinatra/asset_pipeline'

Dir[File.join(File.dirname(__FILE__), 'services', '**/*.rb')].sort.each do |file|
  require file
end

class DeviceCrowler < Sinatra::Base
  set :assets_precompile, %w(*.js *.css *.sass *.png *.jpg *.svg *.eot *.ttf *.woff)
  register Sinatra::AssetPipeline
  
  get '/' do
    slim :'index'
  end

  get '/api/search/:resource', provides: [:json] do
    results = Finder.search(params[:resource], params[:q])
    results.to_json
  end

  get '/api/detail/:resource/:path', provides: [:json] do
    results = Finder.detail(params[:resource], params[:path])
    results.to_json
  end
end
